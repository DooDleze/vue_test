import Vue from 'vue'
import VueRouter from 'vue-router'
import Main_page from '../views/Main_page.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Главная',
    component: Main_page
  },
  {
    path: '/history',
    name: 'История',
    component: () => import('../views/History_page.vue')
  },
  {
    path: '/history/:type',
    name: 'История',
    component: () => import('../views/History_page.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
