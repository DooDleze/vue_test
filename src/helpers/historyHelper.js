export default function historyHelper(params) {
  return {...params, date: Date.now()};
}