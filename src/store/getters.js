export const getters = {
    // Фильтрация и сортировка элементов
    filteredList: state => filter_value => {
        if (filter_value) {
            let value = filter_value.toLowerCase();
            let filtered = [];
            let sorted = [];
            filtered = state.list.filter(list => {
                list.scount = 0;

                let subitems = list.items.filter(function (item) {
                    return item.name.toLowerCase().indexOf(value) != -1;
                })
                list.scount = subitems.length;

                let isitem = list.name.toLowerCase().indexOf(value) != -1;

                if (isitem) {
                    list.scount++;
                }

                if (isitem || subitems.length) {
                    return true;
                }
            });

            sorted = filtered.sort((prev, next) => {
                return (prev.scount < next.scount) ? 1 : -1;
            });

            return sorted;

        } else {
            return state.list.sort((prev, next) => {
                return (prev.id > next.id) ? 1 : -1;
            });
        }
    },

    sortedSelected: state => {
        return state.selected_list.sort((prev, next) => {
            return (prev.id > next.id) ? 1 : -1;
        });
    },

    historyListByType: state => type => {
        if (type) {
            return state.history.filter(list => list.type == type);
        } else {
            return state.history.sort((prev, next) => {
                return (prev.id > next.id) ? 1 : -1;
            });
        }
    }
}