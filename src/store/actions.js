import {HTTP} from '@/services/http';
import historyHelper from '@/helpers/historyHelper';

export const actions = {
    // получает данные с сервера
    getPosts ({ commit }) {
        return HTTP.get('/?list')
        .then( response => commit('setlist', response.data) )
        .catch(function (error) {
            console.log('Ошибка! Не могу связаться с API. ' + error);
        });
    },
    // переносит элемент в правую колонку
    toRight ({ commit, state }, params){
        let itemIndex = state.list.findIndex(el => el.id == params.id);
        let actionItem = state.list[ itemIndex ];
        commit( 'addInListsElement', { storeEl:'selected_list', item: actionItem } );
        commit( 'cutByIndex', { storeEl:'list', id:itemIndex } );
        commit('addInListsElement', {
            storeEl: 'history', 
            item: historyHelper({
                type: 'add',
                item: actionItem
            }) 
        });
    },
    // переносит элемент в левую колонку
    toLeft ({ commit, state }, params){
        let itemIndex = state.selected_list.findIndex(el => el.id == params.id);
        let actionItem = state.selected_list[ itemIndex ];
        commit( 'addInListsElement', { storeEl:'list', item: actionItem } );
        commit( 'cutByIndex', { storeEl:'selected_list', id:itemIndex} );
        commit('addInListsElement', {
            storeEl: 'history', 
            item: historyHelper({
                type: 'del',
                item: actionItem
            }) 
        });
    },
}