export const mutations = {
    setlist (state, list) {
        state.list = list;
    },
    cutByIndex(state, params) {
        state[params.storeEl].splice(params.id, 1);
    },
    addInListsElement(state, params){
        state[params.storeEl].push( params.item );
    },
    setSelectedlist (state, list) {
        state.selected_list = list;
    },
};